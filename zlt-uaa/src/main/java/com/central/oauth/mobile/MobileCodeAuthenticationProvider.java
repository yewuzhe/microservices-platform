package com.central.oauth.mobile;

import com.central.common.constant.SecurityConstants;
import com.central.common.redis.template.RedisRepository;
import com.central.oauth.service.IValidateCodeService;
import com.central.oauth.service.ZltUserDetailsService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author cyq
 */
@Setter
public class MobileCodeAuthenticationProvider implements AuthenticationProvider {
    private ZltUserDetailsService userDetailsService;
    private IValidateCodeService iValidateCodeService;

    @Override
    public Authentication authenticate(Authentication authentication) {
        MobileCodeAuthenticationToken authenticationToken = (MobileCodeAuthenticationToken) authentication;
        String mobile = (String) authenticationToken.getPrincipal();
        String code = (String) authenticationToken.getCredentials();
        Object tempCode = iValidateCodeService.getCode(mobile);
        //验证前端传输的code 是否和数据库code一致

        if(!code.equals(tempCode)){
            throw new BadCredentialsException("验证码错误或验证码过期");
        }

        UserDetails user = userDetailsService.loadUserByMobile(mobile);
        if (user == null) {
            throw new InternalAuthenticationServiceException("手机号错误或用户不存在");
        }

        MobileCodeAuthenticationToken authenticationResult = new MobileCodeAuthenticationToken(user, code, user.getAuthorities());
        authenticationResult.setDetails(authenticationToken.getDetails());
        return authenticationResult;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return MobileCodeAuthenticationToken.class.isAssignableFrom(authentication);
     //   return true;
    }
}
