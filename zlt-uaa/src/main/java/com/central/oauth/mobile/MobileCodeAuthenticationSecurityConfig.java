package com.central.oauth.mobile;

import com.central.oauth.service.IValidateCodeService;
import com.central.oauth.service.ZltUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.stereotype.Component;

/**
 * mobile的相关处理配置
 *
 * @author zlt
 */
@Component
public class MobileCodeAuthenticationSecurityConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
    @Autowired
    private ZltUserDetailsService userDetailsService;
    //
    @Autowired
    private IValidateCodeService iValidateCodeService;


    //不使用PasswordEncoder加密
    @Override
    public void configure(HttpSecurity http) {
        //mobilecode provider
        MobileCodeAuthenticationProvider provider = new MobileCodeAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService);
        provider.setIValidateCodeService(iValidateCodeService);
        http.authenticationProvider(provider);
    }
}
