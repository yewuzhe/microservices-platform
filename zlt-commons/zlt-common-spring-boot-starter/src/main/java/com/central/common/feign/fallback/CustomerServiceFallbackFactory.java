package com.central.common.feign.fallback;

import com.central.common.feign.CustomerService;
import com.central.common.model.Customer;
import com.central.common.model.LoginAppUser;
import com.central.common.model.SysUser;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Service降级工场
 *
 * @author cyq
 * @date 2019/7/8
 */
@Slf4j
@Component
public class CustomerServiceFallbackFactory implements FallbackFactory<CustomerService> {
    @Override
    public CustomerService create(Throwable throwable) {
        return new CustomerService() {
            @Override
            public Customer selectByUsername(String username) {
                log.error("通过用户名查询用户异常:{}", username, throwable);
                return new Customer();
            }

            @Override
            public Customer findByUsername(String customername) {
                log.error("通过用户名查询用户异常:{}", customername, throwable);
                return new Customer();
            }

            @Override
            public Customer findByMobile(String mobile) {
                log.error("通过手机号查询用户异常:{}", mobile, throwable);
                return new Customer();
            }

            @Override
            public Customer findByOpenId(String openId) {
                log.error("通过openId查询用户异常:{}", openId, throwable);
                return new Customer();
            }
        };
    }
}
