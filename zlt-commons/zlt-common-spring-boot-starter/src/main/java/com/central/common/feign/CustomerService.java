package com.central.common.feign;

import com.central.common.constant.ServiceNameConstants;
import com.central.common.feign.fallback.CustomerServiceFallbackFactory;
import com.central.common.model.Customer;
import com.central.common.model.LoginAppUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author cyq
 */
@FeignClient(name = ServiceNameConstants.CUSTOMER_SERVICE, fallbackFactory = CustomerServiceFallbackFactory.class, decode404 = true)
public interface CustomerService {
    /**
     * feign rpc访问远程/customer/name/{username接口
     * 查询用户实体对象Customer
     *
     * @param username
     * @return
     */
    @GetMapping(value = "/customer/name/{username}")
    Customer selectByUsername(@PathVariable("username") String username);

    /**
     * feign rpc访问远程/customer-anon/login接口
     *
     * @param username
     * @return
     */
    @GetMapping(value = "/customer-anon/login", params = "username")
    Customer findByUsername(@RequestParam("username") String username);

    /**
     * 通过手机号查询用户、角色信息
     *
     * @param mobile 手机号
     */
    @GetMapping(value = "/customer-anon/mobile", params = "mobile")
    Customer findByMobile(@RequestParam("mobile") String mobile);

    /**
     * 根据OpenId查询用户信息
     *
     * @param openId openId
     */
    @GetMapping(value = "/customer-anon/openId", params = "openId")
    Customer findByOpenId(@RequestParam("openId") String openId);
}
