package com.central.common.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class passwordUtil {

    public static void main(String[] args) {
        //加入spring security 密码加密
     PasswordEncoder encoder = new BCryptPasswordEncoder();

     System.out.println(encoder.encode("admin_1"));

     System.out.println(encoder.matches("admin_1","$2a$10$rJKrYMOBsj4C2RqHgeWmmOlBgPn.Vm3h1a181dlWVG4CA01F4afWa"));

    }
}
