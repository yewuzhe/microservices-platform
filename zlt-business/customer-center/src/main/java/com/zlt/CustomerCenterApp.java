package com.zlt;

import com.central.common.annotation.EnableLoginArgResolver;
import com.central.search.annotation.EnableSearchClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *cyq
 */
@EnableLoginArgResolver
@EnableDiscoveryClient
@EnableTransactionManagement
@SpringBootApplication
public class CustomerCenterApp {
    public static void main(String[] args) {
        SpringApplication.run(CustomerCenterApp.class, args);
    }
}
