package com.zlt.customer.config;

import com.central.db.config.DefaultMybatisPlusConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author zlt
 * @date 2018/12/10
 */
@Configuration
@MapperScan({"com.zlt.customer.mapper*"})
public class MybatisPlusConfig extends DefaultMybatisPlusConfig {
}
